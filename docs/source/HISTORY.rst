Changelog
=========

0.2.3 (24-03-2014)
------------------
- typo in fonts.css fixed

0.2.2 (24-10-2013)
------------------
- reorganized directory layout
- fixed issues with nested resource directories
  in copyResources()  

0.2.1 (06-10-2013)
------------------
- added browser layer

0.2.0 (03-10-2013)
------------------
- updated documentation 

0.1.9 (30-09-2013)
------------------

- support for new-style collections (Plone 4.3+)
  and images

0.1.8 (30-09-2013)
------------------

- some fixes for folder aggregation

0.1.7 (19-09-2013)
------------------

- images used from within a PDF template file directly
  inside a resource should be marked with
  <img internal-image="true" src="..." />

0.1.6 (17-09-2013)
------------------

- merged https://bitbucket.org/ajung/pp.client-plone/pull-request/2/fixed-umlaut-problem-of-images-and/diff

0.1.5 (21-08-2013)
------------------

- merged https://bitbucket.org/ajung/pp.client-plone/pull-request/1/add-missing-transformer-import-in/diff

0.1.5 (21-08-2013)
------------------

- merged https://bitbucket.org/ajung/pp.client-plone/pull-request/1/add-missing-transformer-import-in/diff

0.1.4 (12-07-2013)
-------------------

- major style and fonts cleanup

0.1.3 (11-07-2013)
-------------------

- various fixes
- Plone 4.0 - 4.2 compatibility

0.1.0 (11-07-2013)
-------------------

- initial release
